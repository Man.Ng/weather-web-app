export const getData = (setWeatherData, location) => fetch(`https://api.openweathermap.org/data/2.5/weather?q=${location}&units=metric&appid=bf95d674bbec582d948d9fe80272e7a7`)
  .then(response => response.json())
  .then(data => {
    setWeatherData(data)
  })
  .catch(error => console.log('error', error));