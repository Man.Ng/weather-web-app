import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './common/main.scss';
import { getData } from './api/getData';
import { Container, Row, Col, FormControl, InputGroup, Button } from 'react-bootstrap';

function App() {

  let [weatherData, setWeatherData] = useState({}),
      [location, setLocation] = useState('London');

  useEffect(() => {
    getData(setWeatherData, 'London')
  }, []);

  const test = () => {
    let inputVal = document.querySelector('.location-input');
    setLocation(inputVal.value)
    getData(setWeatherData, inputVal.value)
  }

  console.log(weatherData)

  return (
    <Container fluid="lg" className="weather-web-app d-flex justify-content-center align-items-center">
      {Object.keys(weatherData).length ? 
        (
          <Row className="d-flex justify-content-center"> 
            <Col sm>
              <Row className="search-row">
                <Col sm>
                  <FormControl
                    className="location-input"
                    placeholder={location}
                  />
                </Col>
                <Col sm>
                  <InputGroup.Append>
                    <Button onClick={() => test()} variant="outline-secondary">Search</Button>
                  </InputGroup.Append>  
                </Col>
              </Row>

              <Row>
                <Col>
                  {weatherData.cod === "404" && <span>{weatherData.message}</span>}

                  {
                    weatherData.cod !== "404" &&
                    weatherData.weather.map(current => {
                      let currentWeather;

                      if (['Clear', 'Clouds', 'Drizzle', 'Rain', 'Snow', 'Thunderstorm'].includes(current.main) ) {
                        currentWeather = current.main;
                      } else {
                        currentWeather = 'Clouds'
                      }

                      return (
                        <div key={current.id}>
                          <span>{Math.round(weatherData.main.temp)}&#8451;</span>
                          <img src={require(`./common/images2/${currentWeather}.svg`).default} alt={current.main}/>
                        </div>
                      )
                    })
                  }
                </Col>
              </Row>
            </Col>
          </Row>
        ) :
        <span>Loading</span>
      }
      
    </Container>
  );
}

export default App;
